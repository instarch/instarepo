#!/bin/bash

source /etc/conf.d/instarepo

inotifywait -r -m -e close_write ${WATCH_PATH} | while read FILE; do
    if [[ "${FILE}" =~ ^.*\.tar\.xz$ ]]; then
        file_path=`echo ${FILE} | gawk '{print $1$3}'`
        filename=`basename ${file_path}`
        cp ${file_path} ${REPO_PATH}

        repo-add ${REPO_PATH}/${REPO_NAME}.db.tar.gz ${REPO_PATH}/${filename}
    fi
done
